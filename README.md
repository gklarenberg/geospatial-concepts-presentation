# Presentation: Introduction to Geospatial Concepts

This html presentation, "Introduction to Geospatial Concepts", was developed to be used when teaching the Carpentries Geospatial curriculum (https://datacarpentry.org/lessons/#geospatial-curriculum and https://datacarpentry.org/geospatial-workshop/). The topics and images all come from the lessons that are available at https://datacarpentry.org/organization-geospatial/. 

This is a first basic attempt at creating an html presentation.

The files and folders are *all* needed to create/run the presentation (images are locally sourced in the code).

Open the Rproject to make sure the links to the images work.