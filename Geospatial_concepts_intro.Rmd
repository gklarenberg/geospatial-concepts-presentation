---
title: "Introduction to Geospatial Concepts "
author: "Geraldine Klarenberg"
date: "05/06/2019"
output: 
  revealjs::revealjs_presentation:
    theme: night
    highlight: tango
    center: true
    transition: slide
    css: custom_cols.css
---

# Introduction to Geospatial Concepts 

## Types of geospatial data
- Raster data: stored as a grid of values, rendered as pixels
- Vector data: represent features, assigned attributes

# Introduction to raster data

---

<figure>
  <img src="images/raster_basic1.png" width=80% height=80% alt="Rasters">
</figure>

---

## Some examples of continuous rasters include:

- Precipitation maps
- Maps of tree height derived LiDAR data
- Elevation values for a region 
- Any others?

## Elevation of Harvard Forest derived from the NEON AOP LiDAR sensor 

<figure>
  <img src="images/HarvardForest_elevation.png" width=40% height=40% alt="Elevation of Harvard Forest from LiDAR sensor">
</figure>

## Some rasters contain categorical data where each pixel presents a discrete class 

<div class="column column1">
<figure>
  <img src="images/USA_landcover.png" width=90% height=90% alt="Landcover map of the USA">
</figure>
</div>
<div class="column column2">

1. Landcover/land-use maps
2. Tree height maps classified as short, medium, and tall trees
3. Elevation maps classified as low, medium, and high elevation 
</div>

## Elevation data for the NEON Harvard Forest field site 
<figure>
  <img src="images/HarvardForest_elevation_classified.png" width=60% height=60% alt="Landcover map of the USA">
</figure>

## Advantages and disadvantages of raster format
- With your neighbor, brainstorm potential advantages and disadvantages of storing data in raster format. Add your ideas to the Etherpad found here:
 https://pad.carpentries.org/2019-05-07_UFBI-IFAS_geospatial
- We will discuss and add any points that weren’t brought up in the small group discussions.

## Pros and cons of raster data 
<div class="column column1">
**Important advantages**

- Representation of continuous surfaces
- Potentially very high levels of detail
- Cell-by-cell calculations can be very fast and efficient 
</div>
<div class="column column2">
**Downsides of raster data**

- Very large file sizes
- Popular formats don’t currently embed metadata well 
- Difficult to represent complex information 
</div>

## Spatial extent
<div class="column column1">
- The spatial extent is the geographic area that the raster data covers. 
- The spatial extent of an R spatial object represents the geographic edge or location that is the furthest north, south, east and west. 
- In other words, extent represents the overall geographic coverage of the spatial object.
</div>
<div class="column column2">
<figure>
  <img src="images/extent_explanation.png" width=90% height=90% alt="Landcover map of the USA">
</figure>
</div>

## Extent challenge
<div class="column column1">
In the image to the right, the dashed boxes around each set of objects seems to imply that the three objects have the same extent. 

- Is this accurate? 
- If not, which object(s) have a different extent?
</div>
<div class="column column2">
<figure>
  <img src="images/extent_explanation.png" width=90% height=90% alt="Landcover map of the USA">
</figure>
</div>

## Resolution
<figure>
  <img src="images/resolution_example.png" width=80% height=80% alt="Example of high to low resolution">
</figure>
A resolution of a raster represents the area on the ground that each pixel of the raster covers. 

The image above illustrates the effect of changes in resolution.

## Raster data format for this workshop 
- GeoTIFF format uses the extension .tif
- .tif stores metadata or attributes about the file as embedded tif tags 
- GeoTIFF is a standard .tif image format with additional spatial (georeferencing) information embedded in the file as tags. 
- The tags include:
1. Extent
2. Resolution
3. Coordinate Reference System (CRS)
4. Values that represent missing data (NoDataValue)

## Multi-band raster data 
<figure>
  <img src="images/multiband_raster.png" width=100% height=100% alt="Multi-band raster data">
</figure>

## Plotting the three bands 
<div class="column column1">
Plotting them individually
<figure>
  <img src="images/RGB_bands_spectral.gif" width=70% height=70% alt="Bands and electromagnetic spectrum">
</figure>

<figure>
  <img src="images/three_bands_separate.png" width=70% height=70% alt="Three bands of a multi-raster plotted separately">
</figure>

</div>

<div class="column column2">
Plotting all three bands
<figure>
  <img src="images/RGB_bands_spectral2.gif" width=60% height=60% alt="Combining RGB bands">
</figure>

<figure>
  <img src="images/three_bands_together.png" width=70% height=70% alt="Raster with three bands plotted together">
</figure>

</div>

## Other types of multi-band raster data
<div class="column column1">
**Time series**

- Same variable
- Same area
- Over time
- <a href=https://datacarpentry.org/r-raster-vector-geospatial/12-time-series-raster/index.html target="_blank"> Raster Time Series Data in R </a>

</div>

<div class="column column2">
**Multi or hyperspectral imagery**

- Image rasters that have four or more bands are called multispectral
- Image rasters of more than 10-15 bands are called hyperspectral 

(If you are interested in working with hyperspectral data, check out the <a href="https://www.neonscience.org/hsi-hdf5-r" target="_blank"> NEON Data Skills Imaging Spectroscopy HDF5 in R </a> tutorial)

</div>

## Key points - raster data
- Raster data is pixelated data where each pixel is associated with a specific location.
- Raster data always has an extent and a resolution.
- The extent is the geographical area covered by a raster.
- The resolution is the area covered by each pixel of a raster.

# Introduction to vector data

## Vector data

<figure>
  <img src="images/pnt_line_poly.png" width=65% height=65% alt="Examples of vector data: point, line, polygon">
</figure>

---

<figure>
  <img src="images/vector_types_challenge.png" width=70% height=70% alt="Find the different vector types">
</figure>

---

## Pros and cons of vector data 
<div class="column column1">
**Important advantages**

- Information on dataset creator's priorities
- Inherent information from geometry
- Multiple attributes per geometry feature
- Efficient data storage

</div>
<div class="column column2">
**Downsides of vector data**

- Potential loss of detail compared to raster
- Potential bias in datasets 
- Calculations on multiple layers involve geometry as well as attributes (slow) 
</div>

## Vector data format for this workshop 
- Shapefile format uses the extension .shp
- .shp stores geographic coordinates of each vertice in the vector, as well as metadata including: 
  * Extent
  * Object type
  * Coordinate reference system (CRS)
  * Other attributes
- Each individual shapefile contains only one vector type (all points, all lines or all polygons)

## Why not both vector and raster data?

---

Format standards are easier to define and maintain

Metadata are easier to define and maintain 

Effects of data manipulations are more predictable 

---

## Key points - vector data
- Vector data structures represent specific features on the Earth’s surface along with attributes of those features.
- Vector objects are either points, lines, or polygons.

# Coordinate Reference Systems

##
A data structure cannot be considered geospatial unless it is accompanied by coordinate reference system (CRS) information

CRS information connects data to the Earth’s surface using a mathematical model.

---

<figure>
  <img src="images/US_CRS_examples.jpg" width=70% height=70% alt="US projected at different CRS">
</figure>

The CRS associated with a dataset tells your mapping software where the raster is located in geographic space.

---

<figure>
  <img src="images/orange-peel-earth.jpg" width=70% height=70% alt="Orange peel analogy to explain CRS">
</figure>

---

## CRS components

- Datum: a model of the shape of the earth. 
  - angular units (i.e. degrees) 
  - starting point (i.e. where is (0,0)?)
  - Common global datums are WGS84 and NAD83

- Projection: a mathematical transformation of the angular measurements on a round earth to a flat surface (units associated with a given projection are usually linear (feet, meters, etc.)

- Additional Parameters (one common additional parameter is a definition of the center of the map)

## Which projection to use?

- Area of minimal distortion?
- What aspect of the data is preserved?

<a href="https://kartoweb.itc.nl/geometrics/Map%20projections/mappro.html" target="_blank"> Department of Geo-Information Processing  </a>  

<a href="http://projectionwizard.org/" target="_blank">  Projection Wizard </a>  

## Describing CRS

- Common systems to describe CRS are EPSG, PROJ, OGC WKT.
- <a href="https://proj4.org/" target="_blank">PROJ</a> is an open-source library for storing, representing and transforming CRS information
- CRS information as a text string of key-value pairs, PROJ4:
  - proj=: the projection of the data
  - zone=: the zone of the data (UTM projection only)
  - datum=: the datum use
  - units=: the units for the coordinates of the data
  - ellps=: the ellipsoid (how the earth’s roundness is calculated) for the data

---

<figure>
  <img src="images/Utm-zones-USA.svg" width=70% height=70% alt="UTM zones of the USA">
</figure>

## CRS challenge

Here is a PROJ4 string for one of the datasets we will use in this workshop:

```{r eval=FALSE}
+proj=utm +zone=18 +datum=WGS84 +units=m +no_defs +ellps=WGS84 +towgs84=0,0,0
```

What projection, zone, datum, and ellipsoid are used for this data?

What are the units of the data?

Using the map above, what part of the United States was this data collected from?

## Geospatial Data Abstraction Library

<a href="https://www.gdal.org/" target="_blank"> GDAL</a> is a set of software tools that translate between almost any geospatial format in common use today 

Contains tools for editing and manipulating both raster and vector files, including reprojecting data to different CRSs

## Metadata
- CRS
- History
- Provenance
- Who is in charge of maintenance
- Appropriate use cases

## Key points - CRS
- All geospatial datasets (raster and vector) are associated with a specific coordinate reference system.
- A coordinate reference system includes datum, projection, and additional parameters specific to the dataset.

# GUI vs CLI

---

**Benefits of GUI**

- Tools are all laid out in front of you
- Complex commands are easy to build
- Don’t need to learn a coding language
- Cartography and visualisation is more intuitive and flexible

**Drawbacks of GUI**

- Low reproducibility - you can’t record your actions and replay
- Most are not designed for batch-processing files
- Limited ability to customise functions or write your own
- Intimidating interface for new users - so many buttons!

---

## GIS via a programming language

*Packages available for R  to wrap geoprocessing libraries and make them easily accessible*

'sf' for working with vector data

'raster' for working with raster data

'rgdal' for an R-friendly GDAL interface

'ggplot2' package for spatial data visualisation.
